import filecmp
import os
import difflib


dir1 = os.listdir('dir1')       # список файлов в dir1
dir2 = os.listdir('dir2')       # список файлов в dir2
match, mismatch, errors = filecmp.cmpfiles('dir1', 'dir2', common=dir1, shallow=False)

print("Shallow comparison:")
print("Список совпадающих файлов :", match)
print("Список несоответствующих файлов :", mismatch)
print("Список ошибок :", errors, "\n")          # файлов нет ни в одном из каталогов, проблемы с доступом и т. д

# diff файлов из списка несоответствующих файлов - mismatch
for file in mismatch:
    with open(f'dir1/{file}') as file_1:
        file_1_text = file_1.readlines()
    with open(f'dir2/{file}') as file_2:
        file_2_text = file_2.readlines()
    # Find and print the diff:
    for line in difflib.unified_diff(
            file_1_text, file_2_text, fromfile=f'dir1/{file}',
            tofile=f'dir2/{file}', lineterm=''):
        print(line)
